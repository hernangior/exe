# Arquivos CSocial

Este repositório contém arquivos para donwload do projeto CSocial, 
Os arquivos estão sempre atualizados, apenas a última versão é mantida.

## Ultima versão - 29/09/2020 🎁 	 
Incluída opção de escolha de certificado digital A1, 
em caso de múltiplos certificados digitais A1 instalados localmente.


## Plataforma e Versões 📚
Disponível apenas para plataforma Windows,
Versões para Windows 32 bits ou 64 bits

## Como usar 📝
Apenas faça a descompactação e use de acordo com a versão desejada.

## Licença 📜
[MIT](https://choosealicense.com/licenses/mit/)